# GitLab CI template for Playwright

This project implements a GitLab CI/CD template to run your automated tests with [Playwright](https://playwright.dev/docs/intro).

## Usage

This template can be used both as a [CI/CD component](https://docs.gitlab.com/ee/ci/components/#use-a-component-in-a-cicd-configuration) 
or using the legacy [`include:project`](https://docs.gitlab.com/ee/ci/yaml/index.html#includeproject) syntax.

### Use as a CI/CD component

Add the following to your `gitlab-ci.yml`:

```yaml
include:
  # 1: include the component
  - component: gitlab.com/to-be-continuous/playwright/gitlab-ci-playwright@1.4.0
    # 2: set/override component inputs
    inputs:
      # ⚠ this is only an example
      project-dir: "e2e"
      review-enabled: "true"
```

### Use as a CI/CD template (legacy)

Add the following to your `gitlab-ci.yml`:

```yaml
include:
  # 1: include the template
  - project: 'to-be-continuous/playwright'
    ref: '1.4.0'
    file: '/templates/gitlab-ci-playwright.yml'

variables:
  # 2: set/override template variables
  # ⚠ this is only an example
  PLAYWRIGHT_PROJECT_DIR: "e2e"
  REVIEW_ENABLED: "true"
```

## `playwright` job

This job starts [Playwright](https://playwright.dev/docs/intro) (functional) tests.

It uses the following variable:

| Input / Variable | Description                                                                                                                 | Default value                               |
| ------------------------ | --------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------- |
| `image` / `PLAYWRIGHT_IMAGE` | The Docker image used to run Playwright.                                                                                    | `mcr.microsoft.com/playwright:latest` |
| `project-dir` / `PLAYWRIGHT_PROJECT_DIR` | The Playwright root project directory (contains the `playwright.config.ts` file)                                            | `.`                                         |
| `extra-args` / `PLAYWRIGHT_EXTRA_ARGS` | Playwright extra [run options](https://playwright.dev/docs/test-cli)                                                        | _none_                                      |
| `review-enabled` / `REVIEW_ENABLED` | Set to `true` to enable Playwright tests on review environments (dynamic environments instantiated on development branches) | _none_ (disabled)                           |
| `node-install-extra-opts` / `NODE_INSTALL_EXTRA_OPTS` | Extra [`npm ci`](https://docs.npmjs.com/cli/ci.html/) options to install project dependencies | _none_ |

In addition to a textual report in the console, this job produces the following reports, kept for one day:

| Report                                                 | Format                                                                      | Usage                                                                                                 |
| ------------------------------------------------------ | --------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------- |
| `$PLAYWRIGHT_PROJECT_DIR/reports/playwright.xunit.xml` | [xUnit](https://github.com/jest-community/jest-junit#readme) test report(s) | [GitLab integration](https://docs.gitlab.com/ee/ci/yaml/artifacts_reports.html#artifactsreportsjunit) |

### base url auto evaluation

By default, the Playwright template tries to auto-determine the [baseURL](https://playwright.dev/docs/api/class-testoptions#test-options-base-url) to use
(i.e. the variable pointing at server under test) by looking either for a `$environment_url` variable or for an
`environment_url.txt` file.
When found, the base url is passed as `$BASE_URL` environment variable and can be reused in your Playwright tests (see below).

Therefore if an upstream job in the pipeline deployed your code to a server and propagated the deployed server url,
either through a [dotenv](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#artifactsreportsdotenv) variable `$environment_url`
or through a basic `environment_url.txt` file, then the Playwright test will automatically be run on this server.

:warning: all our deployment templates implement this design. Therefore even purely dynamic environments (such as review
environments) will automatically be propagated to your Playwright tests.

If you want to retrieve the `$BASE_URL` environment variable evaluated by the template, simply load it from your `playwright.config.ts` file as follows:

```ts
import { defineConfig, devices } from '@playwright/test';

export default defineConfig({
  use: {
    /* retrieve from to-be-continuous or use default (dev) */
    baseURL: process.env.BASE_URL || 'http://localhost:3000',
  },
});
```

### Hook scripts

The Playwright template supports _optional_ **hook scripts** from your project, located in the `$PLAYWRIGHT_PROJECT_DIR` directory to perform additional project-specific logic:

* `pre-playwright.sh` is executed **before** running Playwright,
* `post-playwright.sh` is executed **after** running Playwright (whichever the tests status).
