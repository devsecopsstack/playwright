# [1.4.0](https://gitlab.com/to-be-continuous/playwright/compare/1.3.1...1.4.0) (2024-05-06)


### Features

* add hook scripts support ([6a4673f](https://gitlab.com/to-be-continuous/playwright/commit/6a4673fbe3b94f57b296aa61e1bfe9d278197b90))

## [1.3.1](https://gitlab.com/to-be-continuous/playwright/compare/1.3.0...1.3.1) (2024-05-05)


### Bug Fixes

* **workflow:** disable MR pipeline from prod & integ branches ([fbee66b](https://gitlab.com/to-be-continuous/playwright/commit/fbee66b9ebfbc4c8c02afd09e41b22fa4eb3b353))

# [1.3.0](https://gitlab.com/to-be-continuous/playwright/compare/1.2.0...1.3.0) (2024-2-6)


### Features

* add Node.js extra opts ([395a830](https://gitlab.com/to-be-continuous/playwright/commit/395a8308725c69c914d0af111afb59046c3c8c44))

# [1.2.0](https://gitlab.com/to-be-continuous/playwright/compare/1.1.0...1.2.0) (2024-1-27)


### Features

* migrate to CI/CD component ([374514f](https://gitlab.com/to-be-continuous/playwright/commit/374514f04f23e12c242ad3dde1235c25db8852cc))

# [1.1.0](https://gitlab.com/to-be-continuous/playwright/compare/1.0.0...1.1.0) (2023-12-8)


### Features

* use centralized tracking image (gitlab.com) ([a9ce72e](https://gitlab.com/to-be-continuous/playwright/commit/a9ce72e78d4ef06879c1cd6634bbde438b5c65c7))

# 1.0.0 (2023-11-11)


### Features

* initial template version ([7ce3848](https://gitlab.com/to-be-continuous/playwright/commit/7ce38487f9d01e452e338b649baf41d135205dec))
